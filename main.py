from matplotlib import pyplot as plt
from OnlineData import OnlineData
from SIRD import SIRD

if __name__ == '__main__':
    N = 18.5e7

    # onlineData = OnlineData(
    #     "https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv",
    #     'Bangladesh')
    onlineData = OnlineData("local_data/countries-aggregated.csv", 'Bangladesh')

    S = N - onlineData.confirmed
    I = onlineData.active
    R = onlineData.recovered
    D = onlineData.deaths

    index_values = D.index.values
    S = S.get(index_values)
    I = I.get(index_values)
    R = R.get(index_values)

    T = len(S)
    sird = SIRD(S, I, R, D, N)
    beta_t = []
    gamma_t = []
    mu_t = []
    r0_t = []

    for i in range(1, len(index_values)):
        t = index_values[i]
        t1 = index_values[i - 1]
        beta_t.append(sird.get_beta(t, t1))
        mu_t.append(sird.get_mu(t, t1))
        gamma_t.append(sird.get_gamma(t, t1))
        r0_t.append(sird.get_r0(t, t1))

    plt.plot(index_values[1:], beta_t)
    plt.show()
    plt.plot(index_values[1:], mu_t)
    plt.show()
    plt.plot(index_values[1:], gamma_t)
    plt.show()
    plt.plot(index_values[1:], r0_t)
    plt.show()

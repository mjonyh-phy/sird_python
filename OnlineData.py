import pandas as pd


class OnlineData:
    def __init__(self, csv_route, country=None, threshold=None):
        self.data = None
        self.confirmed = None
        self.deaths = None
        self.active = None
        self.recovered = None
        self.get_data(csv_route)
        self.clean_data(country, threshold)

    def get_data(self, csv_route):
        """ Get the data from a .csv file. This .csv file must contain a time series with
            data for each date in one column, and must contain a 'Country' column.
            Please, refer to https://github.com/datasets/covid-19 for
            an example of the time series format. Returns a pandas.DataFrame object.
            https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv
            Parameters:
            - csv_route : (str) Location to the .csv file (can be a local folder or an URL)"""

        df = pd.read_csv(csv_route).dropna()  # Pandas function to read the .csv file
        df['Date'] = pd.to_datetime(df['Date'])  # Converts date strings to timestamp
        self.data = df

    def clean_data(self, country=None, threshold=None):
        """ Select one (or none) country to study, and returns the same DataFrame read from
            the .csv file, filtered by country, and applying a minimum threshold in order to
            ignore insignificant values. Returns a pandas.Series object.

            Parameters:
            - country: (str) Country to study. Use None for studying global data. (Default None)
            - threshold: (int) Minimum number of cases to consider (Default [0,0,0,0])
        """
        data = self.data
        if threshold is None:
            threshold = [0, 0, 0, 0]  # [Confirmed, Deaths, Active, Recovered]

        if country:
            data = data[data['Country'] == country]  # Filter by country when it's given

        data = data.groupby('Date').sum()  # Group by date and sum cases

        data['Active'] = data['Confirmed'] - data['Recovered'] - data['Deaths']  # Calculates the active cases

        confirmed = data.loc[data['Confirmed'] >= threshold[0], 'Confirmed'].drop_duplicates()
        deaths = data.loc[data['Deaths'] >= threshold[1], 'Deaths'].drop_duplicates()
        active = data.loc[data['Active'] >= threshold[2], 'Active'].drop_duplicates()
        recovered = data.loc[data['Recovered'] >= threshold[3], 'Recovered'].drop_duplicates()

        self.confirmed = confirmed
        self.deaths = deaths
        self.recovered = recovered
        self.active = active

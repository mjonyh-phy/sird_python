class SIRD:
    def __init__(self, s, i, r, d, N):
        self.s = s
        self.i = i
        self.r = r
        self.d = d
        self.N = N

    def get_beta(self, t, t1):
        return -self.N * (self.s[t] - self.s[t1]) / (self.s[t] * self.i[t])

    def get_gamma(self, t, t1):
        return (self.r[t] - self.r[t1]) / self.i[t]

    def get_mu(self, t, t1):
        return (self.d[t] - self.d[t1]) / self.i[t]

    def get_r0(self, t=None, t1=None, beta=None, mu=None, gamma=None):
        if not beta:
            beta = self.get_beta(t, t1)
        if not mu:
            mu = self.get_mu(t, t1)
        if not gamma:
            gamma = self.get_gamma(t, t1)
        return beta / (mu + gamma)
